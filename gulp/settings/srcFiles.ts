import { settings } from "./settings";

export function types(): string[] {
  return [`${src()}/**/*.type.ts`, `${src()}/**/*.model.ts`];
}

export function typesAndCtors(): string[] {
  return [...types(), `${src()}/**/*.ctor.ts`];
}

export function src(): string {
  return `./${settings.src}`;
}
